import React from 'react';

import Header from './components/Header';
import Template from "./components/Template";

import './assets/css/App.css';
import HeaderViewer from "./components/HeaderViewer";

function CMSViewer() {
  return (
    <div className="App">
        <HeaderViewer/>
        <div className="content-box">
            <Header/>
            <Template/>
        </div>
    </div>
  );
}

export default CMSViewer;

import React from 'react';
import { render } from '@testing-library/react';
import CMSNewPage from "./CMS-CreatePage";

test('renders learn react link', () => {
    const { getByText } = render(<CMSNewPage />);
    const linkElement = getByText(/learn react/i);
    expect(linkElement).toBeInTheDocument();
});

//Alle benodigde modules hier
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

//Overige dingen (zoals servicesworkers)
import * as serviceWorker from './serviceWorker';


import './assets/css/index.css';


//Pagina's
import App from './App';
import CMS from './CMS-main';
import CMSViewer from   "./CMS-Viewer";
import CMSCreatePage from "./CMS-CreatePage";



ReactDOM.render(
<BrowserRouter>
    <Switch>
        <Switch>
            <Route path="/index" render={props => <App {...props} />}/>
            <Route path="/CMS" render={props => <CMS {...props} />}/>
            <Route path="/CMS-CreatePage" render={props => <CMSCreatePage {...props}/>}/>
            <Route path="/CMS-Viewer" render={props => <CMSViewer {...props}/>}/>
            <Redirect to="/CMS" />
            <Redirect from="/" to="/CMS" />
        </Switch>
    </Switch>
</BrowserRouter>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

/*eslint-disable*/
import React from "react";

//Header voor de viewer
function HeaderViewer() {

    return (
        <>
            <div class="header">
                <a href="/CMS"><img src="../img/xternit-1.png" class="header-logo" /></a>
                <div class="header-text">Projecten</div>
                <a className="returnButton" href="/CMS">Terug naar het overzicht</a>
            </div>

        </>
    );
}

export default HeaderViewer;

/*eslint-disable*/
import React from "react";

//Fields voor het aanmaken van een nieuwe pagina
function CreatePage() {
    return (
        <>
            <div class="createPage">
                <div class="createPageHeader">Nieuwe Pagina</div>
                <div class="createPageFields">
                    <div class="createPageName">Naam van de pagina</div>
                    <input id="pageName" class="createPageNameInput" placeholder="Naam"/>

                    <div className="createPageDescription">Beschrijving (optioneel)</div>
                    <textarea name="description" className="createPageDescriptionInput" placeholder="Beschrijving"/>
                    <div class="createPageButtons">
                        <a href="/CMS" className="createPageButtonCancel">Annuleren</a>
                        <a href="/CMS" className="createPageButton">Aanmaken</a>
                    </div>
                </div>
            </div>
        </>
    );
}

export default CreatePage;

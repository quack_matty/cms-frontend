import React from 'react';

//Overzicht van de pagina's
function PagesGrid() {
    return (
       <>
           <div className="content-box-title">Pagina's</div>
           <div className="content-box-pages-grid">
               <a href="/CMS-CreatePage" id="newPage" className="content-box-new-page">
                   <div className="content-box-new-page-plus-wrapper">
                       <div className="content-box-new-page-plus-line-horizontal"></div>
                       <div className="content-box-new-page-plus-line-vertical"></div>
                   </div>
                   <div className="content-box-new-page-title"></div>
                   <div className="content-box-new-page-description"></div>
               </a>

               <div id="page" className="content-box-page">
                   <a href="/CMS-Viewer" className="content-box-page-link">VIEW</a>
                   <img class="content-box-page-image" src=""/>
                   <div className="content-box-page-title">Pagina naam</div>
                   <div className="content-box-page-description">Pagina beschrijving</div>
               </div>

           </div>
       </>
    );
}

export default PagesGrid;
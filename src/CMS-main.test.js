import React from 'react';
import { render } from '@testing-library/react';
import CMS from './CMS-main';

test('renders learn react link', () => {
  const { getByText } = render(<CMS />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

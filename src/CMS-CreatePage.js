import React from 'react';

//Header en fields
import Header from './components/Header';
import CreatePage from './components/CreatePage';

import './assets/css/App.css';

function CMSCreatePage() {
    return (
        <div className="App">
            <Header/>
            <div className="content-box">
                <CreatePage/>
            </div>
        </div>
    );
}

export default CMSCreatePage;

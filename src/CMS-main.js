import React from 'react';

//Header en overzicht van pagina's
import Header from './components/Header';
import PagesGrid from "./components/PagesGrid";

import './assets/css/App.css';

function CMS() {
  return (
    <div className="App">
        <Header/>
        <div className="content-box">
            <PagesGrid/>
        </div>
    </div>
  );
}

export default CMS;